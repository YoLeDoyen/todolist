# ToDoList

App to do list en python (back) + bootstrap (front)

## Getting started



## Add your files
```
cd existing_repo
git remote add origin https://gitlab.com/YoLeDoyen/todolist.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/YoLeDoyen/todolist/-/settings/integrations)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Usage


## Support


## Roadmap
- Voire la liste de todolist
- Ajouter un item à la liste
- Supprimer un item de la liste

=>

- Stocker des listes de toDo
- Rayer les items achevés
- Ajouter un choix de couleur pour l'importance

=>

- Associer des images aux items ( ou des liens)

## Authors and acknowledgment
Thank's to Naz Dumanskyy for his tutorial

## Project status
Running
